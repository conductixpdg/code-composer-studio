#include <msp430.h>
#include <stdint.h>
#define MAXCHECKS 10
/*---------------- Global Variables ---------------------*/
volatile char TXData;
int debouncedState;				// DEbounced state of the switches
int state[MAXCHECKS]={0};		// Array that maintains bounce status
int Index = 0;					// Pointer into state
int prevDebouncedState = 0;		// Holds the previous debounced states
/*---------------- Function Called ----------------------*/
int debounceSwitch(void);
int rawPortData(void);
void checkButtons(int x);



int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    // Configure GPIO
    P7SEL0 |= BIT0 | BIT1;
    P7SEL1 &= ~(BIT0 | BIT1);

    // Configure Button RGY and LED
    P5DIR = 0x00;			// Set input for all pins at port 5
    P5IE |= BIT5;			// P5.5 Interrupt Enable
    P5IES &= ~BIT5;			// P5.5 Hi/Lo edge
    P5REN |= BIT5;			// Enable pull up at P5.5
    P5IFG &= ~BIT5;			// Clear Flag

    P4DIR &= 0x00;		// Set input P4.1,2,3
    P4IE |= BIT1 + BIT2 + BIT3;			// P4.1,2,3 Interrupt Enable
    P4IES &= ~(BIT1 + BIT2 + BIT3);		// P4.1,2,3 Hi/Lo edge
    P4REN |= BIT1 + BIT2 + BIT3;		// Enable pull up at P4.1,2,3
    P4IFG &= ~(BIT1 + BIT2 + BIT3);		// Clear Flag

    P1DIR |= BIT1 + BIT0;
    P1OUT = 0x00;
    P2DIR |= BIT5;
    P2OUT = 0x00;


    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;

    // Configure USCI_B2 for I2C mode
    UCB2CTLW0 = UCSWRST;                    // Software reset enabled
    UCB2CTLW0 |= UCMODE_3 | UCSYNC;         // I2C mode, sync mode
    UCB2I2COA0 = 0x48 | UCOAEN;             // own address is 0x48 + enable
    UCB2CTLW0 &= ~UCSWRST;                  // clear reset register
    UCB2IE |= UCTXIE0 | UCSTPIE;            // transmit,stop interrupt enable

    __bis_SR_register(LPM0_bits | GIE);     // Enter LPM0 w/ interrupts
    __no_operation();
}

/*---------------------------------------------------------------------------------
 * --------------------------- I2C Transmission - Slave ---------------------------
 * --------------------------------------------------------------------------------
 */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = EUSCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(EUSCI_B2_VECTOR))) USCI_B2_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(UCB2IV, USCI_I2C_UCBIT9IFG))
    {
        case USCI_NONE:          break;     // Vector 0: No interrupts
        case USCI_I2C_UCALIFG:   break;     // Vector 2: ALIFG
        case USCI_I2C_UCNACKIFG: break;     // Vector 4: NACKIFG
        case USCI_I2C_UCSTTIFG:  break;     // Vector 6: STTIFG
        case USCI_I2C_UCSTPIFG:             // Vector 8: STPIFG
        	TXData = 0;
            UCB2IFG &= ~UCSTPIFG;           // Clear stop condition int flag
            break;
        case USCI_I2C_UCRXIFG3:  break;     // Vector 10: RXIFG3
        case USCI_I2C_UCTXIFG3:  break;     // Vector 12: TXIFG3
        case USCI_I2C_UCRXIFG2:  break;     // Vector 14: RXIFG2
        case USCI_I2C_UCTXIFG2:  break;     // Vector 16: TXIFG2
        case USCI_I2C_UCRXIFG1:  break;     // Vector 18: RXIFG1
        case USCI_I2C_UCTXIFG1:  break;     // Vector 20: TXIFG1
        case USCI_I2C_UCRXIFG0:  break;     // Vector 22: RXIFG0
        case USCI_I2C_UCTXIFG0:             // Vector 24: TXIFG0
            UCB2TXBUF = TXData;
            //if(UCB2TXBUF == TXData){
            	//P1OUT ^= BIT0;
            	//P1OUT |= BIT1;
            //}
            break;
        case USCI_I2C_UCBCNTIFG: break;     // Vector 26: BCNTIFG
        case USCI_I2C_UCCLTOIFG: break;     // Vector 28: clock low timeout
        case USCI_I2C_UCBIT9IFG: break;     // Vector 30: 9th bit
        default: break;
    }
}

/*---------------------------------------------------------------------------------
 * ---------------------------Port 5 interrupt service routine --------------------
 * --------------------------------------------------------------------------------
 */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT5_VECTOR
__interrupt void Port_5(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT5_VECTOR))) Port_5 (void)
#else
#error Compiler not supported!
#endif
{
	TXData = 'A';
	P1OUT ^= BIT1;                            // P1.1 = toggle
	P5IFG &= ~BIT5;                           // P5.5 IFG cleared
	//_delay_cycles(1000);
}
/*---------------------------------------------------------------------------------
 * ---------------------------Port 4 interrupt service routine --------------------
 * --------------------------------------------------------------------------------
 */

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT4_VECTOR
__interrupt void Port_4(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT4_VECTOR))) Port_4 (void)
#else
#error Compiler not supported!
#endif
{
	checkButtons(debounceSwitch());
	if(P4IFG & BIT3){
		TXData = 'R';
		P2OUT ^= BIT5;                            // P1.1 = toggle
		P4IFG &= ~BIT3;                           // P5.5 IFG cleared
	}
	if(P4IFG & BIT2){
		TXData = 'G';
		P2OUT ^= BIT5;                            // P1.1 = toggle
		P4IFG &= ~BIT2;                           // P5.5 IFG cleared
	}
	if(P4IFG & BIT1){
		TXData = 'Y';
		P2OUT ^= BIT5;                            // P1.1 = toggle
		P4IFG &= ~BIT1;                           // P5.5 IFG cleared
	}

}

/*---------------------------------------------------------------------------------
 * --------------------------- Debouncing Buttons --------------------
 * --------------------------------------------------------------------------------
 */
int debounceSwitch(void)
{
	int i, DebouncedORd_State;					// Variables used
	prevDebouncedState = debouncedState;		// Store the old value of debouncedState so it can be OR'd
	state[Index] = rawPortData();				// Assign the raw port data to the State
	++Index;
	debouncedState = 0xFF;						// Assign 0xFF or 11111111 in binary to debouncedState used in AND'ing
	for (i = 0; i < MAXCHECKS; i++) {			// Loop AND's all entries of the array
		debouncedState &= state[i];
	}
	if (Index >= MAXCHECKS) {					// Reset Index
		Index = 0;
	}
	DebouncedORd_State = debouncedState ^ prevDebouncedState;	// OR the final debounce value to change port pin change to logic 1
	return DebouncedORd_State;
}

/*** This function returns the current state of port 1 ANDed with binary 00001110, as only BIT3 and BIT7 are of interest ***/
int rawPortData(void)
{
	int portState = P4IN & 0x0E;
	return portState;
}

/***  This function takes the newly DebouncedORd_State port reading to determine the action required ***/
void checkButtons(int x)
{
	int portState = x;
	switch (portState)
	{
	case BIT1:				// Switch connected to P4.1 has been pressed
			P2OUT ^= BIT5;
		break;
	case BIT2:				// Switch connected to P4.2 has been pressed
			P2OUT ^= BIT5;
		break;
	case BIT3:				// Switch connected to P4.3 has been pressed
			P2OUT ^= BIT5;
			break;
	default:
		break;
	}
}












