#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "driverlib.h"
#include <ff.h>
#include <diskio.h>
#include "HAL_SDCard.h"
#include "cc1101_def.h"
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"
#include "lcd.h"

#define TX_BUF_SIZE 1
#define FREQUENCY   902750
#define CHECK_MSEC_TA 40            // Interval of timer A count to check button state
#define CHECK_MSEC_TB 35            // Interval of timer A count to check button state
#define PRESS_MSEC 10               // stable time before registering pressed
#define RELEASE_MSEC 100            // stable time before registering release

/*---------------- Global Variables ---------------------*/

FIL file;                                               /* Opened file object */
FATFS fatfs;                                            /* File system object */
DIRS dir;                                               /* Directory object   */
FRESULT errCode;                                        /* Error code object  */
FRESULT res;                                            /* Result object      */
UINT bytesRead;                                         /* Bytes read object  */
UINT read;                                              /* Read bytes object  */
Calendar calendar;
FRESULT WriteFile(char*, char*, WORD);

BYTE buffer[32];
int result=1;
volatile unsigned char MST_Data,SLV_Data;
volatile unsigned char buttonPressed;
volatile unsigned char currentState = 0;
volatile unsigned char count;
volatile unsigned char check_rf_end_packet = 0;
unsigned short rx_length;
volatile unsigned char txBuffer[TX_BUF_SIZE];
char TXData;
uint8_t transmitCountBtn_1 = 0;
uint8_t transmitCountBtn_2 = 0;
uint8_t transmitCountBtn_3 = 0;

/*---------------- Global Variables ---------------------*/

/*---------------- Function Called ----------------------*/

void Init_GPIO(void);
void Init_I2C(void);
void Init_CLK(void);
void Init_FAT(void);
void Init_RTC(void);
void Init_TimerA(void);
void Init_TimerB(void);
void Log_SDCard(char data);
void Init_Radio(void);

int main(void) {

    WDT_A_hold(WDT_A_BASE);         // Stop WatchDog Timer
    Init_GPIO();                    // Initialization GPIO
    Init_CLK();                     // Initialization system clocks
    Init_Radio();                   // Initialization Radio Module
    Init_TimerA();                  // Initialization TimerA
    //Init_FAT();                   // Initialization SD Card
    Init_I2C();                     // Initialization I2C communication with Photon Board
    lcd_init();

    send_string("Embedded");
    send_command(0xC0);
    send_string("One Oh One");

    //    __bis_SR_register(GIE);
    //    __bis_SR_register(LPM0_bits | GIE);

    while (1) {

        unsigned char txBuffer[TX_BUF_SIZE];
        char lcdBuffer[50];
        /* configure the timer for a 1 second tick */

        //hal_timer_init(16384);

        /* Initialize the RX chain, receive packet */
        radio_receive_on();
        //radio_wait_for_idle(1024) < 1024)
        if(radio_wait_for_idle(1024) < 1024)
        {
            rx_length = TX_BUF_SIZE;


            /* read content of FIFO */
            radio_read(txBuffer, &rx_length);
            send_command(0x80);
            if (txBuffer[0] == 0x01) {

                if (transmitCountBtn_1 <= 4) {
                    TXData = 'R';
                    transmitCountBtn_1++;
                    sprintf(lcdBuffer, " Btn 1 press: %d", transmitCountBtn_1);

                    send_command(0x01);
                    send_string(lcdBuffer);

                } else if (transmitCountBtn_1 == 5) {
                    send_command(0xC0);
                    send_string("Max!please reset");
                    P1OUT &= ~BIT5;
                }

            }

            if (txBuffer[0] == 0x02){

                if (transmitCountBtn_2 <= 4) {
                    TXData = 'G';
                    transmitCountBtn_2++;
                    sprintf(lcdBuffer, " Btn 2 press: %d", transmitCountBtn_2);

                    send_command(0x01);
                    send_string(lcdBuffer);

                } else if (transmitCountBtn_2 == 5) {
                    send_command(0xC0);
                    send_string("Max!please reset");
                    P1OUT &= ~BIT4;
                }
            }

            if (txBuffer[0] == 0x03){

                if (transmitCountBtn_3 <= 4) {
                    TXData = 'Y';
                    transmitCountBtn_3++;
                    sprintf(lcdBuffer, " Btn 3 press: %d", transmitCountBtn_3);

                    send_command(0x01);
                    send_string(lcdBuffer);

                } else if (transmitCountBtn_3 == 5) {
                    send_command(0xC0);
                    send_string("Max!please reset");
                    P1OUT &= ~BIT2;
                }
            }

            if (txBuffer[0] == 0x04){
                send_command(0x01);
                send_string(" Done! Thank you!");
                transmitCountBtn_1 = 0;
                transmitCountBtn_2 = 0;
                transmitCountBtn_3 = 0;
                P1OUT |= (BIT5 + BIT4 + BIT2);
            }

        }
        else
        {
            // Check to see if we have lost the connection and we need to stop and hold
            radio_idle();

        }
        //        __bis_SR_register(GIE);
        //        __bis_SR_register(LPM0_bits | GIE);
    }
}

/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Radio Register ---------------------------
 * --------------------------------------------------------------------------------
 */

void Init_Radio(void) {

    radio_init(3);

    radio_set_freq(FREQUENCY);

    set_rf_packet_length(TX_BUF_SIZE);

    get_device_id();

}



/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Timer --------------------------
 * --------------------------------------------------------------------------------
 */

void Init_TimerA(void) {
    //Start timer in continuous mode and is source by SMCLK
    TA0CCTL0 = CCIE;                                                // TACCR0 interrupt enabled
    TA0CCR0 = (CHECK_MSEC_TA * 32768 ) / 1000;                    // Counting time
    TA0CTL = TASSEL__SMCLK | MC__UP;                                // ACLK, up_down mode
}

void Init_TimerB(void) {
    //Start timer in continuous mode and is source by SMCLK
    TB0CCTL0 = CCIE;                                                // TACCR0 interrupt enabled
    TB0CCR0 = (CHECK_MSEC_TB * 32768 ) / 1000;                          // Counting time
    TB0CTL = TASSEL__SMCLK | MC__UP;                                // ACLK, up_down mode
}


//******************************************************************************
//
//This is the Timer A0 interrupt vector service routine.
//
//******************************************************************************

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR (void)
{
    TA0CCTL0 &= ~CCIFG;

    volatile unsigned char buttonState = 0x0E;
    currentState = (P3IN & 0x0f);
    if (currentState != buttonState){
        count++;
        if (count >= 4){
            buttonState = currentState;
            if (currentState != 0){
                buttonPressed = 1;
            }
            count = 0;
        }
    } else {
        count = 0;
        buttonPressed = 0;
    }
    __bic_SR_register(LPM0_bits);
}

/*---------------------------------------------------------------------------------
 * --------------------------- Print Variable to SD Card --------------------------
 * --------------------------------------------------------------------------------
 */
void Log_SDCard(char data) {
    //f_mount(0, &fatfs);                           //mount drive number 0
    //f_opendir(&dir, "/");                         //root directory
    //res = f_open(&file, "/LogFile.txt", FA_OPEN_EXISTING | FA_WRITE);
    /* Move to end of the file to append data */
    res = f_lseek(&file, f_size(&file));
    f_printf(&file, "Button %c is PRESSED\n", data);
    f_close(&file);
    f_mount(0,0);
}



/*---------------------------------------------------------------------------------
 * --------------------------- Initialization FAT ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_FAT(void){

    int errCode = -1;

    while (errCode != FR_OK){                               //go until f_open returns FR_OK (function successful)
        errCode = f_mount(0, &fatfs);                       //mount drive number 0
        errCode = f_opendir(&dir, "/");                     //root directory
        errCode = f_open(&file, "/LogFile.txt", FA_OPEN_ALWAYS | FA_WRITE);
        if(errCode != FR_OK) result=0;                      //used as a debugging flag
    }

}


/*---------------------------------------------------------------------------------
 * --------------------------- Initialization GPIO --------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_GPIO(void){
    // Configure GPIO
    P7SEL0 |= BIT0 | BIT1;
    P7SEL1 &= ~(BIT0 | BIT1);

    P1DIR |= BIT1 + BIT2 + BIT0;
    P1OUT &= ~(BIT1 + BIT2 + BIT0);

    /* Button for port 4 */
    P3DIR |= 0xFF;

    P3DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3);      // Set input P3.1,2,3
    P3OUT |= BIT0 + BIT1 + BIT2 + BIT3;          // Set pull-up resistor
    P3REN |= BIT0 + BIT1 + BIT2 + BIT3;        // Enable internal resistor at P3.1,2,3
    P3IE  |= BIT0 + BIT1 + BIT2 + BIT3;         // P3.1,2,3 Interrupt Enable
    P3IES |= (BIT0 + BIT1 + BIT2 + BIT3);     // P3.1,2,3 Hi/Lo edge
    P3IFG &= ~(BIT0 + BIT1 + BIT2 + BIT3);     // Clear Flag

    /* LCD pins Init */
    P8DIR |= 0xFF;
    P8OUT &= 0x00;




    P5SEL1 &= ~(BIT0 | BIT1 | BIT2);        // USCI_B1 SCLK, MOSI, and MISO pin
    P5SEL0 |= (BIT0 | BIT1 | BIT2);

    // Set PJ.4 and PJ.5 as Primary Module Function Input, LFXT.
    GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_PJ,
            GPIO_PIN4 + GPIO_PIN5,
            GPIO_PRIMARY_MODULE_FUNCTION
    );

    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;               // or PMM_unlockLPM5;
}


/*---------------------------------------------------------------------------------
 * --------------------------- Initialization I2C ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_I2C(void) {
    /* Configure USCI_B2 for I2C mode */
    UCB2CTLW0 = UCSWRST;                    // Software reset enabled
    UCB2CTLW0 |= UCMODE_3 | UCSYNC;         // I2C mode, sync mode
    UCB2I2COA0 = 0x48 | UCOAEN;             // own address is 0x48 + enable
    UCB2CTLW0 &= ~UCSWRST;                  // clear reset register
    UCB2IE |= UCTXIE0 | UCSTPIE;            // transmit,stop interrupt enable

}

/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Clock ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_CLK(void) {

    // Set DCO frequency to 8 MHz
    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_6);
    //Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768, 0);
    //Set ACLK=LFXT
    CS_initClockSignal(CS_ACLK, CS_VLOCLK_SELECT, CS_CLOCK_DIVIDER_1); // CS_LFXTCLK_SELECT,CS_VLOCLK_SELECT
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    //Start XT1 with no time out
    CS_turnOnLFXT(CS_LFXT_DRIVE_3);


    /*
    CSCTL0_H = CSKEY_H;
    CSCTL1 = DCOFSEL_6;                         // Set DCO = 8Mhz
    CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;
     */
}

/*---------------------------------------------------------------------------------
 * --------------------------- I2C Transmission - Slave ISR -----------------------
 * --------------------------------------------------------------------------------
 */

#pragma vector = EUSCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
{

    switch(__even_in_range(UCB2IV, USCI_I2C_UCBIT9IFG))
    {
    case USCI_NONE:          break;     // Vector 0: No interrupts
    case USCI_I2C_UCALIFG:   break;     // Vector 2: ALIFG
    case USCI_I2C_UCNACKIFG: break;     // Vector 4: NACKIFG
    case USCI_I2C_UCSTTIFG:  break;     // Vector 6: STTIFG
    case USCI_I2C_UCSTPIFG:             // Vector 8: STPIFG
        TXData = 0;
        UCB2IFG &= ~UCSTPIFG;           // Clear stop condition int flag
        break;
    case USCI_I2C_UCRXIFG3:  break;     // Vector 10: RXIFG3
    case USCI_I2C_UCTXIFG3:  break;     // Vector 12: TXIFG3
    case USCI_I2C_UCRXIFG2:  break;     // Vector 14: RXIFG2
    case USCI_I2C_UCTXIFG2:  break;     // Vector 16: TXIFG2
    case USCI_I2C_UCRXIFG1:  break;     // Vector 18: RXIFG1
    case USCI_I2C_UCTXIFG1:  break;     // Vector 20: TXIFG1
    case USCI_I2C_UCRXIFG0:  break;     // Vector 22: RXIFG0
    case USCI_I2C_UCTXIFG0:             // Vector 24: TXIFG0
        UCB2TXBUF = TXData;
        P1OUT ^= BIT3;
        break;
    case USCI_I2C_UCBCNTIFG: break;     // Vector 26: BCNTIFG
    case USCI_I2C_UCCLTOIFG: break;     // Vector 28: clock low timeout
    case USCI_I2C_UCBIT9IFG: break;     // Vector 30: 9th bit
    default: break;
    }
}

/*---------------------------------------------------------------------------------
 * ---------------------------Port 3 interrupt service routine --------------------
 * --------------------------------------------------------------------------------
 */


#pragma vector=PORT3_VECTOR
__interrupt void Port_3(void)
{
    if(buttonPressed){
        buttonPressed = 0;
        if(P3IFG & BIT0){
            P1OUT ^= BIT0;
            TXData = 'R';
            P3IFG &= ~BIT0;                           // P3.3 IFG cleared
        }
        if(P3IFG & BIT3){
            P1OUT ^= BIT0;
            TXData = 'R';
            P3IFG &= ~BIT3;                           // P3.3 IFG cleared
        }
        if(P3IFG & BIT2){
            P1OUT ^= BIT0;
            TXData = 'G';
            P3IFG &= ~BIT2;                           // P3.2 IFG cleared
        }
        if(P3IFG & BIT1){
            P1OUT ^= BIT0;
            TXData = 'Y';
            P3IFG &= ~BIT1;                           // P3.1 IFG cleared
        }
    }

    P3IFG &= ~(BIT0 + BIT1 + BIT2 + BIT3);
}

