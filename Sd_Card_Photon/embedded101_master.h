#ifndef EMBEDDED101_MASTER_H
#define EMBEDDED101_MASTER_H

/*********************************************************************
 * INCLUDES
 */
#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "driverlib.h"
#include <ff.h>
#include <diskio.h>
#include "HAL_SDCard.h"
#include "cc1101_def.h"
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"
#include "lcd.h"

/*********************************************************************
*  EXTERNAL VARIABLES
*/
/*---------------- Global Variables ---------------------*/

FIL file;                                               /* Opened file object */
FATFS fatfs;                                            /* File system object */
DIRS dir;                                               /* Directory object   */
FRESULT errCode;                                        /* Error code object  */
FRESULT res;                                            /* Result object      */
UINT bytesRead;                                         /* Bytes read object  */
UINT read;                                              /* Read bytes object  */
Calendar calendar;
FRESULT WriteFile(char*, char*, WORD);
BYTE buffer[32];


static volatile unsigned char MST_Data,SLV_Data;
static volatile unsigned char buttonPressed;
static volatile unsigned char check_rf_end_packet = 0;
static volatile char TXData;



/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * CONSTANTS
 */
#define TX_BUF_SIZE 1
#define FREQUENCY   915000
#define CHECK_MSEC_TA 10                // Interval of timer A count to check button state
#define CHECK_MSEC_TB 35                // Interval of timer A count to check button state
#define PRESS_MSEC 10               // stable time before registering pressed
#define RELEASE_MSEC 100                // stable time before registering release


/*********************************************************************
 * MACROS
 */


/*********************************************************************
 * FUNCTIONS
 */

void Init_GPIO(void);
void Init_I2C(void);
void Init_CLK(void);
void Init_FAT(void);
void Init_TimerA(void);
void Init_TimerB(void);
void Log_SDCard(char data);
void Init_Radio(void);


#endif /* EMBEDDED101_MASTER_H */

