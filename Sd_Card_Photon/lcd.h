/* PINS CONNECTION MSP430FR5994 AND LCD
 * -- 4 bits mode ---
 *
 *
 * RS   --->    P3.4 / P8.3    (Register/Data)
 * R/W  --->    P3.5 / P8.2    (Read/Write)
 * E    --->    P3.6 / P8.1    (Enable)
 *
 * DB4  --->    P3.0 / P3.4    (Data Bus 4)
 * DB5  --->    P3.1 / P3.5    (Data Bus 5)
 * DB6  --->    P3.2 / P3.6    (Data Bus 6)
 * DB7  --->    P3.3 / P3.7    (Data Bus 7)
 *
 *
 */



#include <msp430.h>
#define DR P8OUT = P8OUT | BIT3 // define RS high
#define CWR P8OUT = P8OUT & (~BIT3) // define RS low
#define READ P8OUT = P8OUT | BIT2 // define Read signal R/W = 1 for reading
#define WRITE P8OUT = P8OUT & (~BIT2) // define Write signal R/W = 0 for writing
#define ENABLE_HIGH P8OUT = P8OUT | BIT1 // define Enable high signal
#define ENABLE_LOW P8OUT = P8OUT & (~BIT1) // define Enable Low signal

unsigned int i;
unsigned int j;

void delay(unsigned int k)

{
    for(j=0 ; j<=k ; j++)
    {
        for(i=0 ; i<100 ; i++);
    }
}

void data_write(void)
{
    ENABLE_HIGH;
    delay(2);
    ENABLE_LOW;
}

void data_read(void)
{
    ENABLE_LOW;
    delay(2);
    ENABLE_HIGH;
}

void check_busy(void)
{
    /* make P6.3 as input */
    P3DIR &= ~(BIT7);

    while((P3IN & BIT7)==1)
    {
        data_read();
    }

    /* make P6.3 as output */
    P3DIR |= BIT7;
}

void send_command(unsigned char cmd)
{
    check_busy();
    WRITE;
    CWR;

    /* send higher nibble */
    //P3OUT = (P3OUT & 0x0F)|((cmd>>4) & 0x0F);

    P3OUT = (P3OUT & 0x0F) | (cmd & 0xF0);
    /* give enable trigger */
    data_write();

    /* send lower nibble */
    //P3OUT = (P3OUT & 0x0F)|(cmd & 0x0F);

    P3OUT = (P3OUT & 0x0F) | ((cmd<<4) & 0xF0);
    /* give enable trigger */
    data_write();

    __delay_cycles(4000);
}

void send_data(unsigned char data)
{
    check_busy();
    WRITE;
    DR;


    /* send higher nibble */
    //P3OUT = (P3OUT & 0x0F)|((data>>4) & 0x0F);
    P3OUT = (P3OUT & 0x0F) | (data & 0xF0);
    /* give enable trigger */
    data_write();

    /* send lower nibble */
    //P3OUT = (P3OUT & 0x0F)|(data & 0x0F);
    P3OUT = (P3OUT & 0x0F) | ((data<<4) & 0xF0);
    /* give enable trigger */
    data_write();
}

void send_string(char *s)
{
    while(*s)
    {
        send_data(*s);
        s++;
    }
}

void lcd_init(void)
{


    send_command(0x33);
    send_command(0x32);

    /* 4 bit mode */
    send_command(0x28);

    /* clear the screen */
    send_command(0x0E);

    /* display on cursor on */
    send_command(0x01);

    /* increment cursor */
    send_command(0x06);

    /* row 1 column 1 */
    send_command(0x80);
}
