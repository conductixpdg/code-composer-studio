#include <msp430.h> 
#include "driverlib.h"
/*
 * main.c
 */

void Init_GPIO(void);

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    Init_GPIO();

    __bis_SR_register(LPM0_bits | GIE);
    _no_operation();
    return 0;
}


void Init_GPIO(void) {

//    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
//    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN1);
//    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN2);
    P1DIR |= BIT1 + BIT2 + BIT0;
    P1OUT &= ~(BIT1 + BIT2 + BIT0);

    /* Button for port 4 */

//    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P3, GPIO_PIN0);
//    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P3, GPIO_PIN1);
//    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P3, GPIO_PIN2);
//    GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P3, GPIO_PIN3);
//    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN0);
//    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN1);
//    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN2);
//    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN3);
    P3DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3);      // Set input P3.1,2,3
    P3OUT |= BIT0 + BIT1 + BIT2 + BIT3;        // Set pull-up resistor
    P3REN |= BIT0 + BIT1 + BIT2 + BIT3;        // Enable internal resistor at P3.1,2,3
    P3IE  |= BIT0 + BIT1 + BIT2 + BIT3;         // P3.1,2,3 Interrupt Enable
    P3IES |= (BIT0 + BIT1 + BIT2 + BIT3);     // P3.1,2,3 Hi/Lo edge
    P3IFG &= ~(BIT0 + BIT1 + BIT2 + BIT3);     // Clear Flag


    PM5CTL0 &= ~LOCKLPM5;
}


/*---------------------------------------------------------------------------------
 * ---------------------------Port 3 interrupt service routine --------------------
 * --------------------------------------------------------------------------------
 */

#pragma vector=PORT3_VECTOR
__interrupt void Port_3(void)
{
    if(P3IFG & BIT0) {
        P1OUT ^= BIT1 + BIT2 + BIT0;
        P3IFG &= ~BIT0;                           // P3.3 IFG cleared
    }

    if(P3IFG & BIT3) {
        P1OUT ^= BIT1 + BIT2 + BIT0;
        P3IFG &= ~BIT3;                           // P3.3 IFG cleared
    }

    if(P3IFG & BIT2) {
        P1OUT ^= BIT1 + BIT2 + BIT0;
        P3IFG &= ~BIT2;                           // P3.2 IFG cleared
    }

    if(P3IFG & BIT1) {
        P1OUT ^= BIT1 + BIT2 + BIT0;
        P3IFG &= ~BIT1;                           // P3.1 IFG cleared
    }
}
