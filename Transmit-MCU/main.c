//-------------------------------------------------------------------------------------//
//------------------------------- TRANSMIT --------------------------------------------//
//-------------------------------------------------------------------------------------//
#include <msp430.h>
#include <stdint.h>
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "cc1101_def.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"
#include "lcd.h"


// Global Variables

#define TX_BUF_SIZE 1
#define FREQUENCY   902750
unsigned char txBuffer[TX_BUF_SIZE];
unsigned char response;
unsigned char button_pressed;

// Global Functions

void Init_CLK(void);
void Init_Radio(void);
void Init_GPIO(void);


int main(void) {

    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer
    Init_GPIO();
    //Init_CLK();
    Init_Radio();
    lcd_init();

    send_string("Press button to");
    send_command(0xC0);
    send_string("start!");

    while (1)
    {
        P1OUT &= ~(BIT0 + BIT1 + BIT2);
        //__bis_SR_register(GIE);

        while((P3IN&BIT0) != BIT0) {

            /* Clear LCD screen */
            send_command(0x01);

            //hal_timer_init(8000);
            txBuffer[0] = 0x01;
            radio_send(txBuffer, TX_BUF_SIZE);
            radio_wait_for_idle(0);
            radio_receive_on();
            hal_timer_stop();

            P1OUT ^= (BIT0);

            /* Write to LCD */
            send_string("Button 1 is");
            send_command(0xC0);
            send_string("pressed");

        }

        while((P3IN&BIT1) != BIT1){

            /* Clear LCD screen */
            send_command(0x01);

            //hal_timer_init(8000);
            txBuffer[0] = 0x01;
            radio_send(txBuffer, TX_BUF_SIZE);
            radio_wait_for_idle(0);
            radio_receive_on();
            hal_timer_stop();

            P1OUT ^= (BIT0);

            /* Write to LCD */
            send_string("Button 2 is");
            send_command(0xC0);
            send_string("pressed");

        }

        while((P3IN&BIT2) != BIT2){

            /* Clear LCD screen */
            send_command(0x01);

            hal_timer_init(8000);
            txBuffer[0] = 0x03;
            radio_send(txBuffer, TX_BUF_SIZE);
            radio_wait_for_idle(0);
            radio_receive_on();
            hal_timer_stop();

            P1OUT ^= (BIT0);

            /* Write to LCD */
            send_string("Button 3 is");
            send_command(0xC0);
            send_string("pressed");


        }

        while((P3IN&BIT3) != BIT3){

            /* Clear LCD screen */
            send_command(0x01);

            hal_timer_init(8000);
            txBuffer[0] = 0x04;
            radio_send(txBuffer, TX_BUF_SIZE);
            radio_wait_for_idle(0);
            radio_receive_on();
            hal_timer_stop();

            P1OUT ^= (BIT0);

            /* Write to LCD */
            send_string("Reset");
            send_command(0xC0);
            send_string("Done");

        }

    }
}

//--------------------------- Init CLK -----------------------------//

void Init_CLK(void) {

    CSCTL0_H = CSKEY_H;
    CSCTL1 = DCOFSEL_6;                                     // Set DCO = 8Mhz
    CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;
    CSCTL3 = DIVA__1 + DIVS__1 + DIVM__1;

}

//--------------------------- Init Radio -----------------------------//

void Init_Radio(void){

    radio_init(3);
    radio_set_freq(FREQUENCY);
    //radio_set_pwr(0);
    set_rf_packet_length(TX_BUF_SIZE);
    //get_device_id();

}

//--------------------------- Init GPIO -----------------------------//

void Init_GPIO(void) {

    // Configure GPIO
    P7SEL0 |= BIT0 | BIT1;
    P7SEL1 &= ~(BIT0 | BIT1);

    P1DIR |= BIT1 + BIT2 + BIT0;
    P1OUT &= ~(BIT1 + BIT2 + BIT0);

    /* Button for port 4 */
    P3DIR |= 0xFF;

    P3DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3);      // Set input P3.1,2,3
    P3OUT |= BIT0 + BIT1 + BIT2 + BIT3;          // Set pull-up resistor
    P3REN |= BIT0 + BIT1 + BIT2 + BIT3;        // Enable internal resistor at P3.1,2,3

    /* LCD pins Init */
    P8DIR |= 0xFF;
    P8OUT &= 0x00;

    P5SEL1 &= ~(BIT0 | BIT1 | BIT2);        // USCI_B1 SCLK, MOSI, and MISO pin
    P5SEL0 |= (BIT0 | BIT1 | BIT2);

    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}
