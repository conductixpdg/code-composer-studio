/* --COPYRIGHT--,BSD
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include <stdint.h>
#include <msp430.h>
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"
#include "bsl.h"

#define TX_BUF_SIZE 1
#define FREQUENCY	902750

unsigned char txBuffer[TX_BUF_SIZE];
unsigned char response;
uint16_t ii, cc;
unsigned short rx_length;
unsigned int status;

void BSL_Comm_Init(void) {
	/* initialize the radio subsystem */
	radio_init(3);

	/* configure the rx frequency to 902750 */
	radio_set_freq(FREQUENCY);

	// Set the packet length to a fixed packet size
	set_rf_packet_length(TX_BUF_SIZE);
}

/* Reads one byte from BSL target */
uint8_t BSL_getResponse(void) {
	return response;

}

/* Checks if a slave is responding */
uint8_t BSL_slavePresent(void) {
	return 1;
}

/* Sends single I2C Byte (start and stop included) */
void BSL_sendSingleByte(uint8_t ui8Byte) {
	// start the timer
	hal_timer_init(8000);									// 1 second @ 8 MHz

	for(cc=0; cc<10; cc++)									// At most 10 attempts to send
	{

		// put single byte into the payload, clear the rest
		txBuffer[0] = ui8Byte;
		for (ii = 0; ii < TX_BUF_SIZE - 1; ii++) txBuffer[ii + 1] = 0;

		// Transmit packet. The MCU does not exit this until the TX is complete.
		radio_send(txBuffer, TX_BUF_SIZE);
		radio_wait_for_idle(0);         // 0 = no timeout, just wait

		radio_receive_on();              // Change state to RX, receive packet
		// wait until end_of_packet is found or timeout (if packet is not found)
		status = radio_wait_for_idle(1024);

		if(status < 1024) {
			rx_length = TX_BUF_SIZE;
			radio_read(txBuffer, &rx_length);
			response = 0xA5;
			cc = 10;
		}
		else
		{
			radio_idle();
			response = 0xFF;
		}
	}
	hal_timer_stop();
}

/* Sends application image to BSL target (start and stop included)
 * This is a polling function and is blocking. */
void BSL_sendPacket(tBSLPacket tPacket) {
	volatile uint8_t crch, crcl;

	// start the timer
	hal_timer_init(8000);									// 1 second @ 8 MHz

	for(cc=0; cc<10; cc++)									// At most 10 attempts to send
	{

		// put some data into the payload
		txBuffer[0] = 0x80;                            		// Header
		txBuffer[1] = tPacket.ui8Length;                    // Packet length
		txBuffer[2] = tPacket.tPayload.ui8Command;          // Packet command
		if (tPacket.ui8Length > 1) {
			txBuffer[3] = tPacket.tPayload.ui8Addr_L;
			txBuffer[4] = tPacket.tPayload.ui8Addr_M;
			txBuffer[5] = tPacket.tPayload.ui8Addr_H;
			for (ii = 0; ii < (tPacket.ui8Length - 4); ii++) {
				txBuffer[6+ii] = tPacket.tPayload.ui8pData[ii];
			}
			crcl = (uint8_t) (tPacket.ui16Checksum & 0xFF);
			txBuffer[6+ii] = crcl;

			crch = (uint8_t) (tPacket.ui16Checksum >> 8);
			txBuffer[7+ii] = crch;
		}
		else
		{
			crcl = (uint8_t) (tPacket.ui16Checksum & 0xFF);
			txBuffer[3] = crcl;

			crch = (uint8_t) (tPacket.ui16Checksum >> 8);
			txBuffer[4] = crch;
		}

		// Transmit packet. The MCU does not exit this until the TX is complete.
		radio_send(txBuffer, TX_BUF_SIZE);
		radio_wait_for_idle(0);         // 0 = no timeout, just wait

		radio_receive_on();              // Change state to RX, receive packet
		// wait until end_of_packet is found or timeout (if packet is not found)
		status = radio_wait_for_idle(4096);

		if(status < 4096)
		{
			rx_length = TX_BUF_SIZE;
			radio_read(txBuffer, &rx_length);
			response = txBuffer[0];
			cc = 10;
		}
		else
		{
			radio_idle();
			response = 0xFF;
		}
	}
	hal_timer_stop();
	return;
}

void BSL_flush(void) {
	//No flushing for CC1101
	__no_operation();
}
