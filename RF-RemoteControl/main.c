//-------------------------------------------------------------------------------------//
//------------------------------- TRANSMIT --------------------------------------------//
//-------------------------------------------------------------------------------------//

#include <stdint.h>
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "cc1101_def.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"

// Global Variables

#define TX_BUF_SIZE 1
#define FREQUENCY   902750
unsigned char txBuffer[TX_BUF_SIZE];
unsigned char response;
// Global Functions

void Init_CLK(void);
void Init_Radio(void);
void Init_GPIO(void);


int main(void) {
    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer
    Init_GPIO();

    Init_CLK();
    Init_Radio();

    while (1)
    {

            hal_timer_init(8000);


            txBuffer[0] = 0x01;
            //for (ii = 0; ii < TX_BUF_SIZE - 1; ii++) txBuffer[ii + 1] = 0;

            radio_send(txBuffer, TX_BUF_SIZE);
            radio_wait_for_idle(0);         // 0 = no timeout, just wait

            radio_receive_on();
            hal_timer_stop();
            response = get_device_id();
            P1OUT ^= (BIT0);
            __delay_cycles(4000000);
            P1OUT ^= (BIT0);
            __delay_cycles(4000000);

    }
}

//--------------------------- Init CLK -----------------------------//

void Init_CLK(void) {

    CSCTL0_H = CSKEY_H;
    CSCTL1 = DCOFSEL_6;                                     // Set DCO = 8Mhz
    CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;
    CSCTL3 = DIVA__1 + DIVS__1 + DIVM__1;

}

//--------------------------- Init Radio -----------------------------//

void Init_Radio(void){

    radio_init(3);

    radio_set_freq(FREQUENCY);

    set_rf_packet_length(TX_BUF_SIZE);

}

//--------------------------- Init GPIO -----------------------------//

void Init_GPIO(void) {

    P1DIR |= BIT0;
    P1OUT &= ~BIT0;
    PM5CTL0 &= ~LOCKLPM5;
}


//-------------------------------------------------------------------------------------//
//------------------------------- Receiver --------------------------------------------//
//-------------------------------------------------------------------------------------//
/*
#include <msp430.h>
#include <stdint.h>
#include "cc1101_def.h"
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"

#define TX_BUF_SIZE 1
#define FREQUENCY   902750
//---------------- Global Variables ---------------------//
unsigned short rx_length;
unsigned char txBuffer[TX_BUF_SIZE];


int main(void)
{

    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer

    CSCTL0_H = CSKEY_H;
    CSCTL1 = DCOFSEL_6;                         // Set DCO = 8Mhz
    CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;  // set ACLK = VLO
                                                          // MCLK=SMCLK=DCO

    P1DIR |= BIT0;
    P1OUT &= BIT0;
    PM5CTL0 &= ~LOCKLPM5;

    // initialize the radio subsystem /
    radio_init(3);

    // configure the rx frequency to 902750 /
    radio_set_freq(902750);

    // Set the packet length to a fixed packet size
    set_rf_packet_length(TX_BUF_SIZE);

    while(1)
    {
        unsigned char txBuffer[TX_BUF_SIZE];
        // configure the timer for a 1 second tick /
        hal_timer_init(32768);

        // Initialize the RX chain, receive packet/
        radio_receive_on();

        // wait until end_of_packet is found, no timeout
        //status = radio_wait_for_idle(640);

        if(radio_wait_for_idle(1024) < 1024)
        {
            rx_length = TX_BUF_SIZE;
            radio_read(txBuffer, &rx_length);         // read content of FIFO

            if(txBuffer[0] == 0x01) P1OUT ^= BIT0;

        }
        else
        {
            // Check to see if we have lost the connection and we need to stop and hold
            radio_idle();                          // force idle and flush fifos
            // if max_wait == 0 that means we are waiting for first sync burst to appear
        }

    }
}
*/

